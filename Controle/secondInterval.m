function [Out, SetP, t] = secondInterval(out, setp, tin)
%secondInterval Separa o segundo intervalo dos dados
n=1;
while tin(n+1)-tin(n) < 1
    n=n+1;
end

n = n+1;
tFinal = length(tin);

Out = out(n:tFinal);
SetP = setp(n:tFinal);
t = tin(n:tFinal);
end

