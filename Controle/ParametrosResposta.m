function ParametrosResposta(tempo, resposta, entrada, nomeDados, t0, tf)
% ParametrosResposta - Retorna os critérios de desempenho da resposta ao
% degrau fornecida
% tempo - Vetor de tempo
% reposta - Vetor da resposta do sistema
% entrada - Vetor do sinal de entrada
% t0 - Indice da primeira amostra
% tf - Indice da ultima amostra

    if(nargin <= 4)
        t0 = 1;
        tf = length(tempo);
    end

    tempo = tempo(t0:tf);
    resposta = resposta(t0:tf);
    entrada = entrada(t0:tf);
    plot(tempo, resposta, tempo, entrada);

    [~, ~, tempoSubida] = riseTime(resposta, tempo);
    
    tIni = find(entrada > 0, 1);
    [setTime, ~] = settlingTime(resposta(tIni:end), tempo(tIni:end));

    ovSht = 100*(max(resposta) - resposta(end))/resposta(end);
    errSS = entrada(end) - resposta(end);

    crit = ["ts", "tac", "ov", "ssErr"];
    valores = [tempoSubida, setTime, ovSht, errSS];
    dados = table(crit',valores');
    writetable(dados, strcat(nomeDados, ".csv"));
end
