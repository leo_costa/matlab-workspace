%%
close all;
%% Dados simulação
tamanho = size(ResPStep_P_PD_TACO_Fuzzy);

figNames  = ["Fuzzy/Sim/SimKp=4","Fuzzy/Sim/SimKp=4Kd=01",...
             "Fuzzy/Sim/SimKp=4Kt=3","Fuzzy/Sim/SimFuzzy"];
         
figTitles = ["Resposta ao degrau para o sistema com $K_p=4$"            ,...
             "Resposta ao degrau para o sistema com $K_p=4$ e $K_d=0.1$",...
             "Resposta ao degrau para o sistema com $K_p=4$ e $K_t=3$"  ,...
             "Resposta ao degrau para o sistema com controlador Fuzzy"];
         
dados = ResPStep_P_PD_TACO_Fuzzy.Variables;
for i=2: tamanho(1,2)
    figure;
    graficosControle(tTodos, ...
        ResPStep_P_PD_TACO_Fuzzy.Step, dados(:,i), ...
        tTodos(1), tTodos(end), "Posição [V]", figNames(i-1), figTitles(i-1));
end

%% Dados reais SDRT
tamanho = size(RespP_PD_TACO);

figNames = regexprep(figNames, 'Sim', 'Real');

dadosReal = RespP_PD_TACO.Variables;
for i=1: tamanho(1,2)
    figure;
    
    Legenda = {'Degrau','Resposta Real','Resposta Simulada'};
    plot(tTodos, ResPStep_P_PD_TACO_Fuzzy.Step,...
         tTodos, dadosReal(:,i), '-r',tTodos, dados(:,i+1), '--k');
    
    set(gca,'FontSize',12)
    title(figTitles(i),'Interpreter', 'latex','FontSize',14);
    xlabel('Tempo [s]','FontSize',14);
    ylabel('Posição [V]','FontSize',14);
    legend(Legenda,'FontSize',12, 'location', 'southeast');
    grid on;
    
    saveas(gcf,strcat(figNames(i), ".png"));
end