function plotar(X1,Y1,X2,Y2,Titulo,Xlabel,Ylabel,Legenda,Xrise,Yrise)
% figure;
plot(X1, Y1, '--',X2, Y2, '-');
grid on; hold on;
plot(Xrise,Yrise,'m-o', 'MarkerSize',8,'LineWidth',3);
set(gca,'FontSize',12)
title(Titulo,'Interpreter', 'latex','FontSize',14);
xlabel(Xlabel,'FontSize',14);
ylabel(Ylabel,'FontSize',14);
legend(Legenda,'FontSize',12, 'location', 'southeast');
end

