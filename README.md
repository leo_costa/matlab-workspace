# MATLAB Workspace
Este repositório contém algumas funções variadas escritas em MATLAB.

Existem funções para :

* Uso nas aulas de Controle e Servomecanismos 1 e 2
* Para cálculo de um Filtro de Kalman
* Um modelos de testes para um pêndulo invertido feito no Simscape
* Alguns dados para identificação de um modelo em espaço de estados.
