function graficosControle(t,Step,Resp,tini, tend, yName,figName,figTitle)
%graficosControle Gera os gráficos para a matéria de Controle 2 e calcula
% os critérios de desempenho.
%% Inicializacao
    initIndex = find(t >= tini, 1);
    endIndex = find(t >= tend, 1);

    t = t(initIndex:endIndex);
    Step = Step(initIndex:endIndex);
    Resp = Resp(initIndex:endIndex);

%% Plota
    plot(t, Step, t, Resp, '--')

%% Rise time - 10 - 90 % do valor final
    [riseTx,riseTy,tempoSubida] = riseTime(Resp,t);
    fprintf('O tempo de subida é: %.3d s .\n', tempoSubida);

%% Settling time - 2% de variação do valor final
    [settTimeX,settValueX] = settlingTime(Resp,t);
    fprintf('O tempo de acomodação é: %.3d s .\n',settTimeX);

%% Overshoot
    maxX = max(Resp);

    ovX = 100*(maxX - Resp(end))/Resp(end);

    fprintf('O overshoot é: %.3d %%.\n',ovX);

%% Erro SS
    errSSX = (Step(end)-Resp(end));

    fprintf('O erro estacionario é: %.3d V.\n\n',errSSX);

%% Criar graficos
    % X
    Legenda = {'Resposta','Degrau','Tempo de Subida [10~90%]'};
    % subplot(3,1,1);
    plotar(t, Resp, t, Step, figTitle,...
           'Time [s]',yName, Legenda, riseTx, riseTy);
    saveas(gcf,strcat(figName, ".png"));
    % axis([25.9 35 0 2.5]);

%% Exporta criterios de desempenho
    crit = ["ts", "tac", "ov", "ssErr"];
    valores = [tempoSubida, settTimeX, ovX, errSSX];
    dados = table(crit',valores');
    writetable(dados, strcat(figName, ".csv"));

end

