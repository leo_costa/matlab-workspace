function [Out, SetP, t] = firstInterval(out, setp, tin)
%firstInterval Separa o primeiro intervalo dos dados
n=1;
while tin(n+1)-tin(n) < 1
    n=n+1;
end

tFinal = n;

Out = out(1:tFinal);
SetP = setp(1:tFinal);
t = tin(1:tFinal);
end

