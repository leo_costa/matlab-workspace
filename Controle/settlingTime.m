function [settTime,settValue] = settlingTime(Out,t)
%settlingTime Calcula o tempo de acomodação do sistema
Out = abs(Out);
finalValue = Out(end);
value2Pct = finalValue*.02;
is2Pct = false;
for n=1 : length(Out)
    if Out(n) > (finalValue - value2Pct) && Out(n) < (finalValue + value2Pct) && is2Pct == false
        settTime = t(n) - t(1);
        settValue = Out(n);
        is2Pct = true;
    end
    
    if Out(n) < (finalValue - value2Pct) || Out(n) > (finalValue + value2Pct)
        is2Pct = false;
    end
end

end

