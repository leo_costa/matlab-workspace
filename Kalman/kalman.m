C = eye(4);%[1 1 1 1];

D = 0;

Q = 2.3; % A number greater than zero
R = 1;

t = ERForceBallPlacement.Timestamp; %(0:100)';

x=zeros(4,1);     % Initial condition on the state
ye = zeros(length(t),1);
ycov = zeros(length(t),1);
errcov = zeros(length(t),1);

y = ERForceBallPlacement.BallX;   % w = process noise
% yv = y + v;          % v = meas. noise

u = [-0.5; -0.5];

for i=2:length(t)
  dt = t(i) - t(i-1);
  
  A = [1 0 dt 0;
       0 1 0 dt;
       0 0 1 0;
       0 0 0 1];
   
  B = [0.5*dt^2    0   ;
          0    0.5*dt^2;
          dt       0   ;
          0        dt];
  if( i == 2)
      P=B*Q*B';
  end
  % Measurement update
  Mn = P*C'/(C*P*C'+R);
  
  measY = [ERForceBallPlacement.BallX(i) ERForceBallPlacement.BallY(i) ...
           (ERForceBallPlacement.BallX(i) - ERForceBallPlacement.BallX(i-1))/dt ...
           (ERForceBallPlacement.BallY(i) - ERForceBallPlacement.BallY(i-1))/dt];
  
  x = x + Mn * ( measY - C*x);  % x[n|n]
  P = (eye(4)-Mn*C)*P;     % P[n|n]

  aux = C*x;
  ye(i) = aux(1);
  aux2 = C*P*C';
  errcov(i) = aux2(1);

  % Time update
  x = A*x + B*u;        % x[n+1|n]
  P = A*P*A' + B*Q*B';     % P[n+1|n]
end

% subplot(211), 
plot(t,y,'b',t,ye,'r--'),
xlabel('No. of samples'), ylabel('Output')
title('Response with time-varying Kalman filter')
% subplot(212), plot(t,y-yv,'g',t,y-ye,'r--'),
% xlabel('No. of samples'), ylabel('Error')